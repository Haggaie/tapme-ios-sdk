//
//  MoreGamesViewController.h
//  TapMe
//
//  Created by Haggai Elazare on 3/18/14.
//  Copyright (c) 2014 Tap.Pm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tapme.h"

@interface MoreGamesViewController : UIViewController<UIWebViewDelegate>{
    UIButton    *closeButton;
    UILabel     *internetError;
}


@property (assign)                      NSObject    <TMEventDelegate>       *delegate;
@property (nonatomic,strong)IBOutlet    UIWebView                           *webController;
@property (nonatomic,strong)            UIView                              *SpinnerView;

@property (assign)                      BOOL                                isInterstitial;
@property                               BOOL                                shouldRotate;
@property (copy)                        NSString                            *AppID;

@end
