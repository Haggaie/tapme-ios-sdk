//
//  MoreGamesViewController.m
//  TapMe
//
//  Created by Haggai Elazare on 3/18/14.
//  Copyright (c) 2014 Tap.Pm. All rights reserved.
//

#import "MoreGamesViewController.h"
#import <objc/message.h>

@interface MoreGamesViewController ()

@end

@implementation MoreGamesViewController
// Load the framework bundle.
+ (NSBundle *)frameworkBundle {
    static NSBundle* frameworkBundle = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        NSString* mainBundlePath = [[NSBundle mainBundle] resourcePath];
        NSString* frameworkBundlePath = [mainBundlePath stringByAppendingPathComponent:@"Tapme.bundle"];
        frameworkBundle = [NSBundle bundleWithPath:frameworkBundlePath];
    });
    return frameworkBundle;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)viewDidDisappear:(BOOL)animated{
    if ([self.delegate respondsToSelector:@selector(TMViewDidDisappear)]) {
        [_delegate TMViewDidDisappear];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    if ([self.delegate respondsToSelector:@selector(TMViewDidAppear)]) {
        [_delegate TMViewDidAppear];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    else
    {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }
    
    
    
    //No internet connection label
    internetError = [[UILabel alloc] initWithFrame:CGRectMake(self.view.bounds.size.width/2,10, 300, 20)];
    //internetError.center = self.view.center;
    [internetError setAutoresizingMask:UIViewAutoresizingNone];
    [internetError setTextColor:[UIColor whiteColor]];
    [internetError setBackgroundColor:[UIColor clearColor]];
    [internetError setFont:[UIFont fontWithName: @"Trebuchet MS" size: 17.0f]];
    [internetError setText:@"Connection Error."];
    [internetError setHidden:YES];
    
    
    //Create Spinner View
    self.SpinnerView = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 50,self.view.frame.size.height/2 - 50 , 100.0, 100.0)];
    self.SpinnerView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    self.SpinnerView.layer.cornerRadius = 10;
    self.SpinnerView.layer.masksToBounds = YES;
    
   
    
    //Create Loading Label
    UILabel *loadingLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.SpinnerView.frame.size.width/2 - 40, self.SpinnerView.frame.size.height - 30, 80, 30)];
    [loadingLabel setBackgroundColor:[UIColor clearColor]];
    [loadingLabel setTextColor:[UIColor whiteColor]];
    [loadingLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
    [loadingLabel setText:@"Loading...."];
    
    //Create UIActivityIndicatorView object
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activity.center = CGPointMake(self.SpinnerView.frame.size.width/2, self.SpinnerView.frame.size.height/2);;
    [activity startAnimating];
    
    //Close Button
    
    UIImage *closeImage = [UIImage imageWithContentsOfFile:[[[self class] frameworkBundle] pathForResource:@"RPclose_button" ofType:@"png"]];
    closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [closeButton setFrame:CGRectMake(self.view.frame.size.width - 50, 0, 50, 50)];
    [closeButton setBackgroundImage:closeImage forState:UIControlStateNormal];
    [closeButton addTarget:self
                    action:@selector(close:)
          forControlEvents:UIControlEventTouchUpInside];
    [closeButton setAutoresizingMask:UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleLeftMargin];
    
    
    
    [self.SpinnerView addSubview:loadingLabel];
    [self.SpinnerView addSubview:activity];
    
    
    [self.view addSubview:self.SpinnerView];
    
    
    CGRect frame = self.view.bounds;
    
    
    NSArray *strings = [self.AppID componentsSeparatedByString:@"_"];
    
    
    
    NSString *url = [NSString stringWithFormat:@"http://tap.hopy.com/?utm_source=pub_%@&utm_medium=interstitial&utm_term=ad-type&utm_content=app_%@&utm_campaign=sdk",[strings objectAtIndex:0],[strings objectAtIndex:1]];
    
    if (_isInterstitial) {
        url = [NSString stringWithFormat:@"http://tap.hopy.com/?utm_source=pub_%@&utm_medium=interstitial&utm_term=ad-type&utm_content=app_%@&utm_campaign=sdk&one=true",[strings objectAtIndex:0],[strings objectAtIndex:1]];
    }
    
    //Create Web View object
    self.webController = [[UIWebView alloc] initWithFrame:frame];
    self.webController.delegate = self;
    self.webController.backgroundColor = [UIColor colorWithRed:16.0/255.0 green:154.0/255.0 blue:199.0/255.0 alpha:1.0];
    self.webController.opaque = NO;


     self.webController.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin |UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin;
    
    [self.view addSubview:self.webController];
    [self.view addSubview:self.SpinnerView];
    [self.view addSubview:closeButton];
    [self.view addSubview:internetError];
    
    [self.SpinnerView setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin];
    
    [self.webController loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    [self.SpinnerView setHidden:NO];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [self.SpinnerView setHidden:YES];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [self.SpinnerView setHidden:YES];
    closeButton.hidden = NO;
    [internetError setHidden:NO];
    if ([self.delegate respondsToSelector:@selector(TMViewConnectFail:)]) {
        [_delegate TMViewConnectFail:error];
    }
}
#pragma mark UIWEBView  Delegate Methods

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    
    
    NSURL *url = request.URL;
	NSString *urlString = url.absoluteString;
    //  NSLog(@"%@", request.URL.host);
    
    // NSLog(@"%@",urlString);
    
   closeButton.hidden = ![webView.request.URL.host isEqualToString:@"tap.hopy.com"] ? YES : NO;

    //Check which orientation to trigger -----
    if ([urlString rangeOfString:@"gameconversion=1"].location != NSNotFound) {
        if ([self.delegate respondsToSelector:@selector(TMConversion)]) {
            [_delegate TMConversion];
        }
    }
    
    if ([urlString rangeOfString:@"brand="].location != NSNotFound) {
        if ([urlString rangeOfString:@"_orientation_=land"].location != NSNotFound) {
            [self setUIInterfaceOrientation:UIInterfaceOrientationPortrait];
        }
        
        else if ([urlString rangeOfString:@"_orientation_=port"].location != NSNotFound) {
            [self setUIInterfaceOrientation:UIInterfaceOrientationLandscapeRight];
        }
        
        //this release to lock from Auto rotate - only if the page redirect to other game or back to last view
        else{
            [self setShouldRotate:NO];
        }
        
    }
    
    
    return YES;
}
- (void)setUIInterfaceOrientation : (UIInterfaceOrientation)orientation{
    
    //Set new orientation and lock the "shouldAutorotate" method
    
     UIInterfaceOrientation DeviceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if(orientation == UIInterfaceOrientationLandscapeRight || orientation == UIInterfaceOrientationLandscapeLeft )
        if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)]){
            objc_msgSend([UIDevice currentDevice], @selector(setOrientation:), UIInterfaceOrientationPortrait );
            [self setShouldRotate:YES];
        }
    
    if(orientation == UIInterfaceOrientationPortrait)
        if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)]){
            objc_msgSend([UIDevice currentDevice], @selector(setOrientation:), DeviceOrientation);
            [self setShouldRotate:YES];
        }
    
    
}
- (void)close :(id)sender{
    
   
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
