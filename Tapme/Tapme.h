//
//  Tapme.h
//  TapMe
//
//  Created by Haggai Elazare on 3/18/14.
//  Copyright (c) 2014 Tap.Pm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol TMEventDelegate <NSObject>

@optional

/**
 * Called when webview did appear
 * @param n/a
 * @return n/a
 */
- (void)TMViewDidAppear;


/**
 * Called when webview  did disappear
 * @param n/a
 * @return n/a
 */
- (void)TMViewDidDisappear;

/**
 * Called when webview  did fail to load content
 * @param error event The webview  was sent
 * @return n/a
 */
- (void)TMViewConnectFail:(NSError *)error;


/**
 * Called when user converted successfully
 * @param   n/a
 * @return  n/a
 */
- (void)TMConversion;


@end

@interface Tapme : NSObject



/**
 * This method is called to show Tapme More Games webview.
 *
 * @param vController The UIViewController to set as Tapme parentVController_.
 * @param delegate The application delegate.
 * @return n/a
 */
+(void)showMoreGamesWithViewControllerAndAppID: (NSString*)AppID  : (UIViewController*)vController  andDelegate :(id<TMEventDelegate>) delegate;

/**
 * This method is called to show Tapme End Games webview.
 *
 * @param vController The UIViewController to set as Tapme parentVController_.
 * @param delegate The application delegate.
 * @return n/a
 */
+(void)EndGameWithViewControllerAndAppID : (NSString*)AppID       : (UIViewController*)vController  andDelegate :(id<TMEventDelegate>) delegate;

/**
 * This method is called to show Tapme Interstitial webview.
 *
 * @param vController The UIViewController to set as Tapme parentVController_.
 * @param delegate The application delegate.
 * @return n/a
 */
+(void)InterstitialWithViewControllerAndAppID: (NSString*)AppID   : (UIViewController*)vController  andDelegate :(id<TMEventDelegate>) delegate;

/**
 * This method is called to close Tapme  webview.
 * @param n/a
 * @return n/a
 */
+(void)closeTapmeView;

@end
