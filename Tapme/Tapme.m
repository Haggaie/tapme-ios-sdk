//
//  Tapme.m
//  TapMe
//
//  Created by Haggai Elazare on 3/18/14.
//  Copyright (c) 2014 Tap.Pm. All rights reserved.
//

#import "Tapme.h"
#import "MoreGamesViewController.h"


#define xibName [[[self class] frameworkBundle] pathForResource:@"MoreGamesViewController" ofType:@"xib"]

@interface Tapme ()
@end

@implementation Tapme

+(MoreGamesViewController*)instanse{
    static MoreGamesViewController *controller = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
       controller = [[MoreGamesViewController alloc] initWithNibName:xibName bundle:nil];
    });
    return controller;
}

+ (NSBundle *)frameworkBundle {
    static NSBundle* frameworkBundle = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        NSString* mainBundlePath = [[NSBundle mainBundle] resourcePath];
        NSString* frameworkBundlePath = [mainBundlePath stringByAppendingPathComponent:@"Tapme.bundle"];
        frameworkBundle = [NSBundle bundleWithPath:frameworkBundlePath];
    });
    return frameworkBundle;
}


+(void)showMoreGamesWithViewControllerAndAppID: (NSString*)AppID : (UIViewController*)vController  andDelegate :(id<TMEventDelegate>) delegate{
    MoreGamesViewController *moreGames = [[self class] instanse];//[[MoreGamesViewController alloc] initWithNibName:xibName bundle:nil];
    [moreGames setDelegate:delegate];
    [moreGames setAppID:AppID];
    [moreGames setIsInterstitial:NO];
    [vController presentViewController:moreGames animated:YES completion:nil];

}

+(void)EndGameWithViewControllerAndAppID : (NSString*)AppID       : (UIViewController*)vController  andDelegate :(id<TMEventDelegate>) delegate{
   // NSString *xib = [[[self class] frameworkBundle] pathForResource:xibName ofType:@"xib"];
    MoreGamesViewController *moreGames = [[self class] instanse];//[[MoreGamesViewController alloc] initWithNibName:xib bundle:nil];
    [moreGames setDelegate:delegate];
    [moreGames setAppID:AppID];
    [moreGames setIsInterstitial:NO];
    [vController presentViewController:moreGames animated:YES completion:nil];

}

+(void)InterstitialWithViewControllerAndAppID: (NSString*)AppID   : (UIViewController*)vController  andDelegate :(id<TMEventDelegate>) delegate{
    MoreGamesViewController *moreGames = [[self class] instanse];//[[MoreGamesViewController alloc] initWithNibName:xibName bundle:nil];
    [moreGames setDelegate:delegate];
    [moreGames setAppID:AppID];
    [moreGames setIsInterstitial:YES];
    [vController presentViewController:moreGames animated:YES completion:nil];

}

+(void)closeTapmeView{
   [[[self class] instanse] dismissViewControllerAnimated:YES completion:nil] ;
}


@end
